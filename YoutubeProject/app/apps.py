from django.apps import AppConfig
from . import templates
from .canalyt import YTCnl
from urllib.request import urlopen


class YoutubeConfig(AppConfig):
    #default_auto_field = "django.db.models.BigAutoField"
    name = "app"

    def ready(self):
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urlopen(url)
        canal = YTCnl(xmlStream)
        templates.selectable = canal.videos()


