
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('plantilla', views.plantilla),
    path('main_css', views.main_css),
    path('<str:id>', views.videos)
]
