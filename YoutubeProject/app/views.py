from django.http import HttpResponse
from django.middleware.csrf import get_token
from django.shortcuts import render

from . import templates

# Create your views here.
text = """
body {
    margin: 10px 20% 50px 70px;
    font-family: sans-serif;
    color: red;
    background:aqua;
    }"""
def index(request):
    if request.method == 'POST':
        if 'id' in request.POST:
            if request.POST.get('select'):
                move_v(from_list=templates.selectable, to_list=templates.selected, id=request.POST['id'])
            elif request.POST.get('deselect'):
                move_v(from_list=templates.selected, to_list=templates.selectable, id=request.POST['id'])
    csrf_token = get_token(request)
    selected = to_HTML(name='deselect', list=templates.selected, action='Deseleccionar', token=csrf_token)
    selectable = to_HTML(name='select', list=templates.selectable, action='Seleccionar', token=csrf_token)
    htmlBody = templates.PAGE.format(selected=selected, selectable=selectable)
    return HttpResponse(htmlBody)

def to_HTML(name, list, action, token):
    html = ""
    for video in list:
        html = html + templates.VIDEO.format(link=video['link'], title=video['title'], id=video['id'], name=name,
                                   action=action, token=token, image=video['image'], published=video['published'],
                                   description=video['description'], url=video['url'])
    return html

def videos(request, id):
    encontrado = None
    if request.method == 'GET':
        for i, video in enumerate(templates.selected):
            if video['id'] == id:
                encontrado = video
        if encontrado != None:
            htmlBody = templates.INFORMACION.format(video=encontrado)
            return HttpResponse(htmlBody)
    htmlBody = templates.ERROR.format(id=id)
    return HttpResponse(htmlBody)

def move_v(from_list, to_list, id):
    found = None
    for i, video in enumerate(from_list):
        if video['id'] == id:
            found = from_list.pop(i)
    if found:
        to_list.append(found)

def plantilla(request):
    return render(request, "/home/alumnos/jose/PycharmProjects/django-youtube1/YoutubeProject/app/templates/plantilla1.html")
def main_css(request):
    return HttpResponse(content_type=text)


